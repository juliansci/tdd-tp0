package ar.fiuba.tecnicas.rockpaperscissors.solucion1;

public class Paper implements Combat {

    public Combat vs(Rock rock) {
        return this;
    }

    public Combat vs(Paper paper) {
        return this;
    }

    public Combat vs(Scissor scissor) {
        return scissor;
    }

}

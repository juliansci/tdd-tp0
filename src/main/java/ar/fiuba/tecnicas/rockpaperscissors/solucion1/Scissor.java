package ar.fiuba.tecnicas.rockpaperscissors.solucion1;

public class Scissor implements Combat {

    public Combat vs(Rock rock) {
        return rock;
    }

    public Combat vs(Paper paper) {
        return this;
    }

    public Combat vs(Scissor scissor) {
        return this;
    }

}

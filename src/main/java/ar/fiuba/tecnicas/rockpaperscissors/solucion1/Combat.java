package ar.fiuba.tecnicas.rockpaperscissors.solucion1;

public interface Combat {

    
    public Combat vs(Rock rock);

    public Combat vs(Paper paper);

    public Combat vs(Scissor scissor);

}

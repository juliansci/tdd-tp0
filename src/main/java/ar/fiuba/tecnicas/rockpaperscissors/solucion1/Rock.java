package ar.fiuba.tecnicas.rockpaperscissors.solucion1;

public class Rock implements Combat {

    public Combat vs(Rock rock) {
        return this;
    }

    public Combat vs(Paper paper) {
        return paper;
    }

    public Combat vs(Scissor scissor) {
        return this;
    }

}

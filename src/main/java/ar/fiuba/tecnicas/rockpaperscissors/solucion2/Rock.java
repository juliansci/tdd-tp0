package ar.fiuba.tecnicas.rockpaperscissors.solucion2;

public class Rock implements Combat {

    public Combat vs(Combat combat) {
        return combat.vs(this);
    }

    public Combat vs(Paper paper) {
        return paper;
    }

    public Combat vs(Rock rock) {
        return this;
    }

    public Combat vs(Scissor scissor) {
        return this;
    }

}

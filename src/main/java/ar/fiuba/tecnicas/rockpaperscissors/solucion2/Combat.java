package ar.fiuba.tecnicas.rockpaperscissors.solucion2;

public interface Combat {

    public Combat vs(Combat combat);
 
}

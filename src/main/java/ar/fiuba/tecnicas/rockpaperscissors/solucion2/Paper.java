package ar.fiuba.tecnicas.rockpaperscissors.solucion2;

public class Paper implements Combat {

    public Combat vs(Combat combat) {
        return combat.vs(this);
    }

    public Combat vs(Paper paper) {
        return this;
    }

    public Combat vs(Rock rock) {
        return this;
    }

    public Combat vs(Scissor scissor) {
        return scissor;
    }

}
